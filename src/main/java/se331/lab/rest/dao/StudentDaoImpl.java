package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;
@Profile("LabDao")
@Repository
@Slf4j
public class StudentDaoImpl implements StudentDao{
    List<Student> students;
    public StudentDaoImpl(){
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(1l)
                .studentId("SE-010")
                .name("Koy")
                .surname("Ruchawin")
                .gpa(2.01)
                .image("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcToE79KnS2FBHRxjfMN3e6G9UEPWTvGj-ohJ-_GyD5EpyGjqFQo")
                .penAmount(1)
                .description("The best dish in eastern of Thailand")
                .build());
        this.students.add(Student.builder()
                .id(2l)
                .studentId("SE-011")
                .name("Nui ")
                .surname("Amphon")
                .gpa(3.52)
                .image("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQKab3DbUy148REso_Dz-7GtFqT3IJ2mNPi8rnFthrwsxe2DDaQ")
                .penAmount(2)
                .description("Na-moooooooo")
                .build());
        this.students.add(Student.builder()
                .id(3l)
                .studentId("SE-012")
                .name("O")
                .surname("Kung")
                .gpa(1.49)
                .image("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTXFQ-50ma4iSugQYbaYkhG0q0vdQZKE6ckheUgM0dI5Sqb73Pa")
                .penAmount(3)
                .description("Baby Baby Baby oooooo")
                .build());

    }

    @Override
    public List<Student> getAllStudent() {
        log.info("LabDao is called");
        return students;
    }

    @Override
    public Student findById(Long studentId) {
        return students.get((int) (studentId -1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) students.size());
        students.add(student);
        return student;
    }
}
