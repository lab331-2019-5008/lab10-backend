package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;

import se331.lab.rest.repository.CourseRepository;
import se331.lab.rest.repository.LecturerRepository;
import se331.lab.rest.repository.StudentRepository;

@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    LecturerRepository lecturerRepository;
    @Autowired
    CourseRepository courseRepository;

    @Override
    @Transactional
    public void run(ApplicationArguments args) throws Exception {
        Student student1 = Student.builder()
                .id(1l)
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The minister")
                .gpa(3.59)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/tu.jpg?alt=media&token=f16b5c1c-fbea-4d98-9fa3-732b69a1bae8")
                .penAmount(15)
                .description("The great man ever!!!!")
                .build();
        Student student2 = Student.builder()
                .id(2l)
                .studentId("SE-002")
                .name("Cherprang ")
                .surname("BNK48")
                .gpa(4.01)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/cherprang.png?alt=media&token=2e6a41f3-3bf0-4e42-ac6f-8b7516e24d92")
                .penAmount(2)
                .description("Code for Thailand")
                .build();
        Student student3 = Student.builder()
                .id(3l)
                .studentId("SE-003")
                .name("Nobi")
                .surname("Nobita")
                .gpa(1.77)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/nobita.jpg?alt=media&token=16e30fb0-9904-470f-b868-c35601df8326")
                .penAmount(0)
                .description("Welcome to Olympic")
                .build();
        Student student4 = Student.builder()
                .id(4l)
                .studentId("SE-010")
                .name("Koy")
                .surname("Ruchawin")
                .gpa(2.01)
                .image("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcToE79KnS2FBHRxjfMN3e6G9UEPWTvGj-ohJ-_GyD5EpyGjqFQo")
                .penAmount(1)
                .description("The best dish in eastern of Thailand")
                .build();
        Student student5 = Student.builder()
                .id(5l)
                .studentId("SE-011")
                .name("Nui ")
                .surname("Amphon")
                .gpa(3.52)
                .image("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQKab3DbUy148REso_Dz-7GtFqT3IJ2mNPi8rnFthrwsxe2DDaQ")
                .penAmount(2)
                .description("Na-moooooooo")
                .build();
        Student student6 = Student.builder()
                .id(6l)
                .studentId("SE-012")
                .name("O")
                .surname("Kung")
                .gpa(1.49)
                .image("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTXFQ-50ma4iSugQYbaYkhG0q0vdQZKE6ckheUgM0dI5Sqb73Pa")
                .penAmount(3)
                .description("Baby Baby Baby oooooo")
                .build();
        studentRepository.save(student1);
        studentRepository.save(student2);
        studentRepository.save(student3);
        studentRepository.save(student4);
        studentRepository.save(student5);
        studentRepository.save(student6);

        Lecturer lecturer1 = Lecturer.builder()
                .name("Chartchai")
                .surname("Duangsa-ard")
                .build();
        Lecturer lecturer2 = Lecturer.builder()
                .name("Jayakrit")
                .surname("Hirisajja")
                .build();
        Lecturer lecturer3 = Lecturer.builder()
                .name("Kong")
                .surname("Kasit")
                .build();

        lecturerRepository.save(lecturer1);
        lecturerRepository.save(lecturer2);
        lecturerRepository.save(lecturer3);

        Course course1 = Course.builder()
                .courseId("953331")
                .courseName("Component Based Software Dev")
                .content("Nothing just for fun")
                .build();
        Course course2 = Course.builder()
                .courseId("953xxx")
                .courseName("X project")
                .content("Do not know what to study")
                .build();
        Course course3 = Course.builder()
                .courseId("001001")
                .courseName("English for Everyday life")
                .content("No content")
                .build();
        Course course4 = Course.builder()
                .courseId("953100")
                .courseName("Learning Through Activity")
                .content("Learn Everythings")
                .build();

        courseRepository.save(course1);
        courseRepository.save(course2);
        courseRepository.save(course3);
        courseRepository.save(course4);

        lecturer1.getAdvisees().add(student1);
        lecturer1.getAdvisees().add(student2);
        lecturer1.getAdvisees().add(student3);
        lecturer1.getAdvisees().add(student4);
        lecturer1.getAdvisees().add(student5);
        lecturer1.getAdvisees().add(student6);

        student1.setAdvisor(lecturer1);
        student2.setAdvisor(lecturer1);
        student3.setAdvisor(lecturer1);
        student4.setAdvisor(lecturer1);
        student5.setAdvisor(lecturer1);
        student6.setAdvisor(lecturer1);

        //953xxx = c2, 953100 = c4
        student1.getEnrolledCourse().add(course2);
        student2.getEnrolledCourse().add(course2);
        student3.getEnrolledCourse().add(course2);
        student4.getEnrolledCourse().add(course2);
        student5.getEnrolledCourse().add(course2);
        student6.getEnrolledCourse().add(course2);

        course2.getStudents().add(student1);
        course2.getStudents().add(student2);
        course2.getStudents().add(student3);
        course2.getStudents().add(student4);
        course2.getStudents().add(student5);
        course2.getStudents().add(student6);

        student1.getEnrolledCourse().add(course4);
        student2.getEnrolledCourse().add(course4);
        student3.getEnrolledCourse().add(course4);
        student4.getEnrolledCourse().add(course4);
        student5.getEnrolledCourse().add(course4);
        student6.getEnrolledCourse().add(course4);

        course4.getStudents().add(student1);
        course4.getStudents().add(student2);
        course4.getStudents().add(student3);
        course4.getStudents().add(student4);
        course4.getStudents().add(student5);
        course4.getStudents().add(student6);

        course3.setLecturer(lecturer1);
        course4.setLecturer(lecturer3);

        lecturer1.getCourse().add(course3);
        lecturer3.getCourse().add(course4);

        studentRepository.save(student1);
        studentRepository.save(student2);
        studentRepository.save(student3);
        studentRepository.save(student4);
        studentRepository.save(student5);
        studentRepository.save(student6);

    }



}

